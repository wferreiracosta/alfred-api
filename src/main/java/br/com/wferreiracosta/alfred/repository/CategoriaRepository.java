package br.com.wferreiracosta.alfred.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.wferreiracosta.alfred.domain.Categoria;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Integer>{

	boolean existsByNome(String nome);

	@Query("SELECT DISTINCT obj FROM Categoria obj INNER JOIN obj.produtos prod WHERE prod.id = :id")
	List<Categoria> findByProdutosId(@Param("id") Integer id);
    
}