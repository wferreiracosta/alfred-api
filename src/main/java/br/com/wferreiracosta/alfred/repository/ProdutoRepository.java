package br.com.wferreiracosta.alfred.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.wferreiracosta.alfred.domain.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer>{
    boolean existsByNome( String nome );
}
