package br.com.wferreiracosta.alfred.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.wferreiracosta.alfred.domain.Categoria;
import br.com.wferreiracosta.alfred.domain.Produto;
import lombok.Data;

@Data
public class ProdutoCategoriaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String nome;
    private Double preco;

    private List<Categoria> categorias = new ArrayList<>();

    public ProdutoCategoriaDTO() {
    }

    public ProdutoCategoriaDTO(Produto produto) {
        this.id = produto.getId();
        this.nome = produto.getNome();
        this.preco = produto.getPreco();
    }

    public ProdutoCategoriaDTO(Produto produto, List<Categoria> lista) {
        this.id = produto.getId();
        this.nome = produto.getNome();
        this.preco = produto.getPreco();
        this.categorias = lista;
    }
}