package br.com.wferreiracosta.alfred.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.wferreiracosta.alfred.domain.Produto;
import br.com.wferreiracosta.alfred.repository.ProdutoRepository;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository repo;

    public Produto find(Integer id) {
        Optional<Produto> obj = this.repo.findById(id);
        return obj.orElse(null);
    }

    public List<Produto> findAll() {
        return this.repo.findAll();
    }
}
