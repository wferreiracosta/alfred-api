package br.com.wferreiracosta.alfred.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.wferreiracosta.alfred.domain.Categoria;
import br.com.wferreiracosta.alfred.repository.CategoriaRepository;
import br.com.wferreiracosta.alfred.service.exception.ObjectNotFoundException;

@Service
public class CategoriaService {

    @Autowired
    private CategoriaRepository repo;

    public Categoria find(Integer id) {
        Optional<Categoria> obj = this.repo.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException(
            "Objeto não encontrado! Id: "+id+", Tipo: "+Categoria.class.getName()
        ));
    }

    public List<Categoria> findAll() {
		return repo.findAll();
	}

	public List<Categoria> findByProdutosId(Integer id) {
        return this.repo.findByProdutosId(id);
	}

}
