package br.com.wferreiracosta.alfred;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlfredApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(AlfredApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
	}
}
