package br.com.wferreiracosta.alfred.config;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import br.com.wferreiracosta.alfred.service.DataBaseService;

@Configuration
@Profile("test")
public class TestConfig {
    @Autowired
	private DataBaseService dbService;

	@Bean
	public boolean instantiateDatabase() throws ParseException {
		dbService.instantiateTestDatabase();
		return true;
	}
	
}