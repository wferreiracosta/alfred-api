package br.com.wferreiracosta.alfred.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.wferreiracosta.alfred.domain.Categoria;
import br.com.wferreiracosta.alfred.domain.Produto;
import br.com.wferreiracosta.alfred.dto.ProdutoCategoriaDTO;
import br.com.wferreiracosta.alfred.service.CategoriaService;
import br.com.wferreiracosta.alfred.service.ProdutoService;

@RestController
@RequestMapping(value = "/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService service;
	@Autowired
    private CategoriaService categoriaService;
	
    @RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAll() {
		List<Produto> objLista = service.findAll();
		List<ProdutoCategoriaDTO> objDTOLista = new ArrayList<>();
		for(Produto objProduto : objLista){
			List<Categoria> listaCategorias = this.categoriaService.findByProdutosId(objProduto.getId());
			objDTOLista.add(new ProdutoCategoriaDTO(objProduto,listaCategorias ));
		}
		return ResponseEntity.ok().body(objDTOLista);
	}

	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer id) {
		Produto obj = this.service.find(id);
		List<Categoria> lista = this.categoriaService.findByProdutosId(obj.getId());
		ProdutoCategoriaDTO objDTO = new ProdutoCategoriaDTO(obj, lista);
		return ResponseEntity.ok().body(objDTO);
	}
}
