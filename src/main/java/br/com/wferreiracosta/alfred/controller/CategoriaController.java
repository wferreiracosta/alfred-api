package br.com.wferreiracosta.alfred.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.wferreiracosta.alfred.domain.Categoria;
import br.com.wferreiracosta.alfred.dto.CategoriaDTO;
import br.com.wferreiracosta.alfred.service.CategoriaService;

@RestController
@RequestMapping(value = "/categorias")
public class CategoriaController {

	@Autowired
	private CategoriaService service;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAll() {
		List<Categoria> objLista = service.findAll();
		List<CategoriaDTO> listDto = objLista.stream().map(obj -> new CategoriaDTO(obj)).collect(Collectors.toList());  
		return ResponseEntity.ok().body(listDto);
	}

	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer id) {
		Categoria obj = this.service.find(id);
		CategoriaDTO objDTO = new CategoriaDTO(obj);
		return ResponseEntity.ok().body(objDTO);
	}

}