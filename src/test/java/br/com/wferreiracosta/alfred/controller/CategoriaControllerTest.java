package br.com.wferreiracosta.alfred.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.wferreiracosta.alfred.AlfredApplicationTests;

public class CategoriaControllerTest  extends AlfredApplicationTests {

	static String CATEGORIA_API = "/categorias";
    private MockMvc mockMvc;

    @Autowired
    private CategoriaController categoriaController;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(categoriaController).build();
    }

	@Test
	public void buscarTodasCategorias() throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
            .get(CATEGORIA_API);
		
        this.mockMvc.perform(request)
            .andExpect(status().isOk());
    }
    
    @Test
	public void buscarCategoriaPorId() throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
            .get(CATEGORIA_API+"/1");
		
        this.mockMvc.perform(request)
            .andExpect(status().isOk())
            .andExpect(jsonPath("id").isNotEmpty())
            .andExpect(jsonPath("id").value(1))
            .andExpect(jsonPath("nome").value("Informática"));
    }

}