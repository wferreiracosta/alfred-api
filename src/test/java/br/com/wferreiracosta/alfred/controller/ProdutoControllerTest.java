package br.com.wferreiracosta.alfred.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.wferreiracosta.alfred.AlfredApplicationTests;

public class ProdutoControllerTest  extends AlfredApplicationTests {

	static String PRODUTO_API = "/produtos";
    private MockMvc mockMvc;

    @Autowired
    private ProdutoController produtoController;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(produtoController).build();
    }

	@Test
	public void buscarTodosProdutos() throws Exception {
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(PRODUTO_API);
		
        this.mockMvc.perform(request)
            .andExpect(status().isOk());
    }
    
    @Test
	public void buscarProdutoPorId() throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get(PRODUTO_API+"/1");
		
        this.mockMvc.perform(request)
            .andExpect(status().isOk())
            .andExpect(jsonPath("id").isNotEmpty())
            .andExpect(jsonPath("id").value(1))
            .andExpect(jsonPath("nome").value("Computador"))
            .andExpect(jsonPath("preco").value(2000.00));
	}
}