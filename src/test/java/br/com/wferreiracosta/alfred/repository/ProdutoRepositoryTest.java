package br.com.wferreiracosta.alfred.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.wferreiracosta.alfred.domain.Produto;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
public class ProdutoRepositoryTest {
    @Autowired
    TestEntityManager entityManager;

    @Autowired
    ProdutoRepository repository;

    @Test
    public void retornarTrueSeIdExiste() {
        Produto produto = new Produto(null, "Laptop", 2000.00);
        entityManager.persist(produto);

        boolean existsById = repository.existsById(produto.getId());

        assertTrue(existsById);
    }

    @Test
    public void retornarTrueSeNomeExiste() {
        Produto produto = new Produto(null, "Laptop", 2000.00);
        entityManager.persist(produto);

        boolean existsByNome = repository.existsByNome(produto.getNome());

        assertTrue(existsByNome);
    }

    @Test
    public void buscarPorId() {
        Produto produto = new Produto(null, "Laptop", 2000.00);
        entityManager.persist(produto);

        Optional<Produto> findById = repository.findById(produto.getId());

        assertTrue(findById.isPresent());
    }

    @Test
    public void salvarProduto(){
        Produto produto = new Produto(null, "Laptop", 2000.00);
        Produto savedProduto = repository.save(produto);
        assertNotNull(savedProduto.getId());
    }

    @Test
    public void apagarProduto(){
        Produto produto = new Produto(null, "Laptop", 2000.00);
        entityManager.persist(produto);
        Produto foundProduto = entityManager.find(Produto.class, produto.getId());

        repository.delete(foundProduto);

        Produto deleteProduto = entityManager.find(Produto.class, produto.getId());
        assertNull(deleteProduto);
    }
}
