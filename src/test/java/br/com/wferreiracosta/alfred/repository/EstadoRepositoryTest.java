package br.com.wferreiracosta.alfred.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.wferreiracosta.alfred.domain.Estado;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
public class EstadoRepositoryTest {
    @Autowired
    TestEntityManager entityManager;

    @Autowired
    EstadoRepository repository;

    @Test
    public void retornarTrueSeIdExiste() {
        Estado estado = new Estado(null, "São Paulo");
        entityManager.persist(estado);
        boolean existsById = repository.existsById(estado.getId());
        assertTrue(existsById);
    }

    @Test
    public void buscarPorId() {
        Estado estado = new Estado(null, "São Paulo");
        entityManager.persist(estado);
        Optional<Estado> findById = repository.findById(estado.getId());
        assertTrue(findById.isPresent());
    }

    @Test
    public void salvarEstado(){
        Estado estado = new Estado(null, "São Paulo");
        Estado savedEstado = repository.save(estado);
        assertNotNull(savedEstado.getId());
    }

    @Test
    public void apagarEstado(){
        Estado estado = new Estado(null, "São Paulo");
        entityManager.persist(estado);
        Estado foundEstado = entityManager.find(Estado.class, estado.getId());
        repository.delete(foundEstado);
        Estado deleteEstado = entityManager.find(Estado.class, estado.getId());
        assertNull(deleteEstado);
    }
}
