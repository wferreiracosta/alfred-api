package br.com.wferreiracosta.alfred.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.wferreiracosta.alfred.domain.Categoria;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
public class CategoriaRepositoryTest {
    @Autowired
    TestEntityManager entityManager;

    @Autowired
    CategoriaRepository repository;

    @Test
    public void retornarTrueSeIdExiste() {
        Categoria categoria = new Categoria(null, "Informática");
        entityManager.persist(categoria);
        boolean existsById = repository.existsById(categoria.getId());
        assertTrue(existsById);
    }

    @Test
    public void retornarTrueSeNomeExiste() {
        Categoria categoria = new Categoria(null, "Informática");
        entityManager.persist(categoria);
        boolean existsByNome = repository.existsByNome(categoria.getNome());
        assertTrue(existsByNome);
    }

    @Test
    public void buscarPorId() {
        Categoria categoria = new Categoria(null, "Informática");
        entityManager.persist(categoria);
        Optional<Categoria> findById = repository.findById(categoria.getId());
        assertTrue(findById.isPresent());
    }

    @Test
    public void salvarCategoria(){
        Categoria categoria = new Categoria(null, "Informática");
        Categoria savedCategoria = repository.save(categoria);
        assertNotNull(savedCategoria.getId());
    }

    @Test
    public void apagarCategoria(){
        Categoria categoria = new Categoria(null, "Informática");
        entityManager.persist(categoria);
        Categoria foundCategoria = entityManager.find(Categoria.class, categoria.getId());
        repository.delete(foundCategoria);
        Categoria deleteCategoria = entityManager.find(Categoria.class, categoria.getId());
        assertNull(deleteCategoria);
    }
}
