package br.com.wferreiracosta.alfred.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.wferreiracosta.alfred.domain.Cidade;
import br.com.wferreiracosta.alfred.domain.Estado;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
public class CidadeRepositoryTest {
    @Autowired
    TestEntityManager entityManager;

    @Autowired
    CidadeRepository repository;

    @Test
    public void retornarTrueSeIdExiste() {
        Estado estado = new Estado(null, "São Paulo");
        Cidade cidade = new Cidade(null, "São Paulo", estado);
        entityManager.persist(estado);
        entityManager.persist(cidade);
        boolean existsById = repository.existsById(cidade.getId());
        assertTrue(existsById);
    }

    @Test
    public void buscarPorId() {
        Estado estado = new Estado(null, "São Paulo");
        Cidade cidade = new Cidade(null, "São Paulo", estado);
        entityManager.persist(cidade);
        Optional<Cidade> findById = repository.findById(cidade.getId());
        assertTrue(findById.isPresent());
    }

    @Test
    public void salvarCidade(){
        Estado estado = new Estado(null, "São Paulo");
        Cidade cidade = new Cidade(null, "São Paulo", estado);
        Cidade savedCidade = repository.save(cidade);
        assertNotNull(savedCidade.getId());
    }

    @Test
    public void apagarCidade(){
        Estado estado = new Estado(null, "São Paulo");
        Cidade cidade = new Cidade(null, "São Paulo", estado);
        entityManager.persist(cidade);
        Cidade foundCidade = entityManager.find(Cidade.class, cidade.getId());
        repository.delete(foundCidade);
        Cidade deleteCidade = entityManager.find(Cidade.class, cidade.getId());
        assertNull(deleteCidade);
    }
}
